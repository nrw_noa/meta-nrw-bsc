SUMMARY = "Litecell15 Linux image with BTS and minimal GSM core network software installed"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

DEPENDS = "litecell15-bts-image (>= 0.9.3)"

inherit gitver-repo

require ../../meta-nrw-bts/recipes-core/images/litecell15-bts-image.bb

REPODIR   = "${THISDIR}"
REPOFILE  = ""
PR       := "r${REPOGITFN}"
PV       := "${REPOGITT}-${REPOGITN}"

IMAGE_NAME = "${PN}-${PV}-${PR}-${DATETIME}"
NITB_LAYER_VERSION := "${PV}-${PR}"


install_lc15_manifest_append() {
    printf "           NITB : %s\n" "${NITB_LAYER_VERSION}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
}

IMAGE_INSTALL+="packagegroup-nitb \
		"

inherit core-image
