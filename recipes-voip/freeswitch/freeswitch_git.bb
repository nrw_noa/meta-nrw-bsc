SUMMARY="Freeswitch Open-Source Telephony Platform"
HOMEPAGE="https://www.freeswitch.org/"
LICENSE="MPL-1.0"
LIC_FILES_CHKSUM="file://docs/COPYING;md5=c878b7dac31225fc5b03ff92d985f147"

RDEPENDS_${PN} += "python python-distutils perl libpq curl libopencore-amr"

DEPENDS += "sqlite3 lua zlib jpeg libpng curl libpcre speex \
libyaml freetype portaudio-v19 openldap expat libxml2 virtual/gettext python3 \
libsmpp34 tiff libldns flite libopencore-amr"

FS_USER = "freeswitch"
FS_GROUP = "daemon"

EXTRA_OECONF = " \
--disable-libyuv \
--disable-libvpx \
--without-erlang \
--disable-zrtp \
--disable-core-pgsql-support \
--disable-core-libedit-support \
--with-perl=no \
--with-java=no \
--with-python=${PYTHON} \
STAGING_DIR=${STAGING_DIR_NATIVE} \
"

BRANCH = "v1.6"

SRC_URI = "git://freeswitch.org/stash/scm/fs/freeswitch.git;protocol=https;branch=${BRANCH} \
file://0001-fix-python3-detection-when-crosscompiling.patch \
file://modules.conf \
file://freeswitch.service \
file://smpp_reconnect.patch \
file://python3.patch \
file://py3.h \
file://0001-use-opencoreamr-in-mod-amr.patch \
file://0002-osmocom-amr-sip-sdp-specific.patch \
file://0003-change-default-bitrate-and-octet-align-configurable-in-mod-amr.patch \
"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "freeswitch_git.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

TARGET_VER = "1.6.16"
SRCREV = "v${TARGET_VER}"
PV = "${TARGET_VER}+git${SRCPV}"

inherit autotools-brokensep pkgconfig python3native useradd

CACHED_CONFIGUREVARS = " \
ac_cv_prog_PERL=false \
ac_cv_have_perl=no \
ac_cv_prog_PHP=false \
ac_cv_have_php=no \
ac_cv_prog_PHP_CONFIG=false \
ac_cv_have_php_config=no \
ac_cv_gcc_supports_w_no_unused_result=no \
"

do_configure_prepend() {
	# install py3.h file from CCM
	cp ${WORKDIR}/py3.h ${S}/src/mod/languages/mod_python

	cd ${S}
	./bootstrap.sh

	# Override default modules selection
	cp ${WORKDIR}/modules.conf ${S}

	# Remove references to build host filesystem
	sed -i 's!/usr/include /usr/local/include /usr/include/libxml2 /usr/local/include/libxml2!${STAGING_INCDIR}/libxml2!' libs/spandsp/configure.ac

	# Use system libexpat
	sed -i 's!--with-expat=builtin!--with-expat=${STAGING_DIR}/${MACHINE}/usr!' libs/apr-util/configure.gnu

	# Use system libtiff
	sed -i 's/enable-builtin-tiff/enable-builtin-tiff=no/' libs/spandsp/configure.gnu
}

do_configure_append() {
	# freeswitch expects a libtool script called 'libtool'
	ln -sf ${HOST_PREFIX}libtool libtool
}

do_install_append() {
	if ${@bb.utils.contains('DISTRO_FEATURES','systemd','true','false',d)}; then
		install -d ${D}/${systemd_unitdir}/system
		install -m 644 ${WORKDIR}/freeswitch.service ${D}/${systemd_unitdir}/system
	fi

	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	mv ${D}${sysconfdir}/freeswitch ${D}${sysconfdir}/defaultconfig/config
	ln -sf /mnt/rom/user/config/freeswitch ${D}${sysconfdir}/freeswitch

	# Don't use IPv6 profiles
	rm -rf ${D}${sysconfdir}/defaultconfig/config/freeswitch/sip_profiles/external-ipv6
	rm -f ${D}${sysconfdir}/defaultconfig/config/freeswitch/sip_profiles/external-ipv6.xml
	rm -f ${D}${sysconfdir}/defaultconfig/config/freeswitch/sip_profiles/internal-ipv6.xml

	# Remove unused dialplan
	rm -f ${D}${sysconfdir}/defaultconfig/config/freeswitch/dialplan/features.xml
	rm -f ${D}${sysconfdir}/defaultconfig/config/freeswitch/dialplan/default/*.xml
	rm -f ${D}${sysconfdir}/defaultconfig/config/freeswitch/dialplan/public/*.xml

	# Override freeswitch configuration files
	cp -R ${THISDIR}/files/config/* ${D}${sysconfdir}/defaultconfig/config/freeswitch
}

PYTHON_SITE_DIR = "${PYTHON_SITEPACKAGES_DIR}"
LIBTOOL = "${STAGING_BINDIR_CROSS}/${HOST_SYS}-libtool"
export PYTHON_SITE_DIR
export BUILD_SYS
export HOST_SYS
export STAGING_INCDIR
export STAGING_LIBDIR
export LIBTOOL

FILES_${PN}-dbg += "/usr/lib/freeswitch/mod/.debug"
FILES_${PN} += "${PYTHON_SITEPACKAGES_DIR} /run"
FILES_${PN} += "${@bb.utils.contains('DISTRO_FEATURES', 'systemd', '${systemd_unitdir}/system/freeswitch.service', '', d)}"

USERADD_PACKAGES = "${PN}"
USERADD_PARAM_${PN} = "-r -g ${FS_GROUP} ${FS_USER}"
