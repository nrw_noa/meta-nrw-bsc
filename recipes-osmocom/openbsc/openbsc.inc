DESCRIPTION = "OpenBSC a Free Software GSM BaseStationController"
HOMEPAGE = "http://openbsc.osmocom.org/"
LICENSE = "AGPLv3+"
LIC_FILES_CHKSUM = "file://COPYING;md5=73f1eb20517c55bf9493b7dd6e480788"

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "openbsc.inc"
INC_PR   := "r${REPOGITFN}"

inherit autotools pkgconfig systemd

EXTRA_OECONF += "--enable-smpp"


