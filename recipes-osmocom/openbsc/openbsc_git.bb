# This recipe is adapted from original openbsc recipe in meta-telephony
# to meet our requirements

require ${PN}.inc

DEPENDS = "libdbi (= 0.8.3) libosmocore libosmo-abis libosmo-netif openggsn libsmpp34 c-ares libpcap"
RDEPENDS_osmo-nitb = "libdbd-sqlite3"

S = "${WORKDIR}/git/openbsc"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "openbsc_git.bb"
PR       := "${INC_PR}.${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_BRANCH  = "${@ 'nrw/litecell15-next' if d.getVar('NRW_BSC_DEVEL', False) == "next" else 'nrw/litecell15'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://${NRW_NOA_MIRROR}/openbsc.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/litecell15"
REL_SRCREV  = "e90844d198af87817f9e716596439dfadea4edb9"
REL_SRCURI := "git://${NRW_NOA_MIRROR}/openbsc.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_SRCURI}'}"

SRC_URI += " \
	file://osmo-nitb.cfg \
	file://osmo-nitb.service \
	file://osmo-sgsn.cfg \
	file://osmo-sgsn.service \
	file://osmo-nitb \
	"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_configure_prepend () {
	# there is a bug in tests suite during compilation
	# we skip tests suite compilation to avoid build error for now
 	sed -i 's/tests//g' ${S}/Makefile.am
	sed -i '/tests\//d' ${S}/configure.ac
	# generate version for autotool
	echo "${PV}" > ${S}/.tarball-version
}

do_install () {
	install -d ${D}${sysconfdir}/osmocom
        install -d ${D}${sysconfdir}/defaultconfig
        install -d ${D}${sysconfdir}/defaultconfig/config
        install -d ${D}${sysconfdir}/defaultconfig/config/osmocom
	install -d ${D}/var/lib
	install -d ${D}${bindir}

	install -m 0660 ${WORKDIR}/osmo-nitb.cfg ${D}${sysconfdir}/defaultconfig/config/osmocom
	install -m 0660 ${WORKDIR}/osmo-sgsn.cfg ${D}${sysconfdir}/defaultconfig/config/osmocom
	install -m 0660 ${WORKDIR}/osmo-nitb ${D}${sysconfdir}/defaultconfig/config/osmocom

	install -m 0755 ${WORKDIR}/build/src/osmo-nitb/osmo-nitb ${D}${bindir}
	install -m 0755 ${WORKDIR}/build/src/gprs/osmo-sgsn ${D}${bindir}

	# create symbolic links
	ln -sf /mnt/rom/user/config/osmocom/osmo-nitb.cfg ${D}${sysconfdir}/osmocom/osmo-nitb.cfg
	ln -sf /mnt/rom/user/config/osmocom/osmo-nitb ${D}${sysconfdir}/osmocom/osmo-nitb
	ln -sf /mnt/rom/user/config/osmocom/osmo-sgsn.cfg ${D}${sysconfdir}/osmocom/osmo-sgsn.cfg
	ln -sf /mnt/storage/var/lib/osmocom/ ${D}/var/lib/osmocom

	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	install -m 0644 ${WORKDIR}/osmo-nitb.service ${D}${systemd_unitdir}/system/
	install -m 0644 ${WORKDIR}/osmo-sgsn.service ${D}${systemd_unitdir}/system/

       # enable systemd on sysinit
       ln -sf ../osmo-nitb.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
       ln -sf ../osmo-sgsn.service ${D}${systemd_unitdir}/system/multi-user.target.wants/

}


PACKAGES =+ "osmo-nitb osmo-sgsn"

FILES_osmo-nitb = "${bindir}/osmo-nitb \
		/var/lib/osmocom \
		${sysconfdir}/defaultconfig/config/osmocom/osmo-nitb.cfg \
		${sysconfdir}/osmocom/osmo-nitb.cfg \
		${sysconfdir}/defaultconfig/config/osmocom/osmo-nitb \
		${sysconfdir}/osmocom/osmo-nitb \
		${systemd_unitdir}/system/osmo-nitb.service \
		${systemd_unitdir}/system/multi-user.target.wants/osmo-nitb.service \
		"
FILES_osmo-sgsn = "${bindir}/osmo-sgsn \
		${sysconfdir}/defaultconfig/config/osmocom/osmo-sgsn.cfg \
		${sysconfdir}/osmocom/osmo-sgsn.cfg \
		${systemd_unitdir}/system/osmo-sgsn.service \
		${systemd_unitdir}/system/multi-user.target.wants/osmo-sgsn.service \
		"
