SUMMARY="Festival Lite embeddable text-to-speech engine"
HOMEPAGE="http://www.speech.cs.cmu.edu/flite/"
LICENSE="BSD"
LIC_FILES_CHKSUM="file://COPYING;md5=416ef1ca5167707fe381d7be33664a33"

SRC_URI = "git://freeswitch.org/stash/scm/sd/libflite.git;branch=master;protocol=https"
SRCREV = "59ae81c506bb6165fd474feaa50020dea4f63bd5"

S = "${WORKDIR}/git"

EXTRA_OECONF = "--with-audio=none --enable-sockets --enable-shared"

inherit autotools-brokensep pkgconfig

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_git.bb"
PR       := "r${REPOGITFN}"

PV = "git${SRCPV}"

do_install_append() {
	chown -R root:root ${D}${libdir}
}
