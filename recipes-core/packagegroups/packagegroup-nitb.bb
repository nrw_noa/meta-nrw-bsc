DESCRIPTION = "BSC default Package Groups"
LICENSE = "BSD"

inherit packagegroup

PACKAGES = "\
	packagegroup-nitb \
	"

RDEPENDS_packagegroup-nitb = "\
	osmo-nitb \
	osmo-sgsn \
	openggsn \
	iptables \
	freeswitch \
	osmo-sip-connector \
	freeswitch-sounds-music-8000 \
	freeswitch-sounds-en-us-callie-8000 \
	"
