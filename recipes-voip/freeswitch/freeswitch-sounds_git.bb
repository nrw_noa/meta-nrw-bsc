SUMMARY="Sounds for Freeswitch Open-Source Telephony Platform"
HOMEPAGE="https://www.freeswitch.org/"
# We use CC-BY version 3.0 which is having the closest release date mentioned in source
# license file 2012-05-15 (https://freeswitch.org/stash/projects/FS/repos/freeswitch-sounds/browse/en/LICENSE). 
# Referece: https://wiki.creativecommons.org/wiki/License_Versions
LICENSE="CC-BY-3.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/CC-BY-3.0;md5=dfa02b5755629022e267f10b9c0a2ab7"


DEPENDS = "freeswitch"

BRANCH = "master"
SRCREV = "${AUTOREV}"

SRC_URI = "git://freeswitch.org/stash/scm/fs/freeswitch-sounds.git;protocol=https;branch=${BRANCH} \
"

S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "${PN}_git.bb"
PR       := "r${REPOGITFN}"

PV = "git${SRCPV}"

inherit perlnative

do_configure () {
	# run resampling Perl script for all supported sampling rates
	${S}/dist.pl	
}

# Only EN/US voice files sampled at 8000 Hz will include in the generated packages
SAMPLE_RATE = "8000"

# Use version defined in dist.pl Perl script
MUSIC_VER = "1.0.52"
VOICE_VER = "1.0.52"

do_install () {
	install -d ${D}${datadir}/freeswitch
	install -d ${D}${datadir}/freeswitch/sounds

	tar -xf ${S}/${PN}-music-${SAMPLE_RATE}-${MUSIC_VER}.tar.gz -C ${D}${datadir}/freeswitch/sounds
	tar -xf ${S}/${PN}-en-us-callie-${SAMPLE_RATE}-${VOICE_VER}.tar.gz -C ${D}${datadir}/freeswitch/sounds

	chown -R root:root ${D}${datadir}/freeswitch/sounds
}

PACKAGES =+ "${PN}-music-${SAMPLE_RATE} ${PN}-en-us-callie-${SAMPLE_RATE}"

FILES_${PN}-music-${SAMPLE_RATE} = "${datadir}/freeswitch/sounds/music"
FILES_${PN}-en-us-callie-${SAMPLE_RATE} = "${datadir}/freeswitch/sounds/en"
