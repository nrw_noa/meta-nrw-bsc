SUMMARY = "DNS programming library"
SECTION = "console/network"
HOMEPAGE = "http://www.nlnetlabs.nl/ldns/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE;md5=34330f15b2b4abbbaaa7623f79a6a019"

DEPENDS = "openssl"

SRC_URI = "git://git.nlnetlabs.nl/ldns.git;branch=master"
SRCREV = "470934b7d8cb16e75de4da9fc4da7ab7ddc58cd1"
S = "${WORKDIR}/git"

EXTRA_OECONF = "--without-pyldns --without-pyldnsx --without-p5-dns-ldns --with-ssl=${STAGING_DIR_HOST}${prefix} --disable-dane-verify"

inherit pkgconfig autotools-brokensep

do_configure_prepend() {
    libtoolize --quiet --copy --force --install
}

do_configure_append() {
    ln -sf ${HOST_PREFIX}libtool libtool
}
