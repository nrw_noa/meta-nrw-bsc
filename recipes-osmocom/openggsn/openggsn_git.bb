DESCRIPTION = "OpenGGSN a Free Software GGSN"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=8ca43cbc842c2336e835926c2166c28b"

S = "${WORKDIR}/git"
inherit autotools pkgconfig systemd

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "openggsn_git.bb"
PR       := "r${REPOGITFN}"

REPODIR   = "${THISDIR}/files"
REPOFILE  = ""
PR       := "${PR}.${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

BRANCH = "master"
#we use version 0.92 for now, we will be able to use HEAD as soon as the libosmocore is in sync with HEAD of master branch
SRCREV = "0.92"
SRC_URI = "git://git.osmocom.org/openggsn;branch=${BRANCH}"

DEPENDS = "libosmocore"
RDEPENDS_${PN} += "kernel-module-tun kernel-module-nf-nat-masquerade-ipv4 iptables"

IF="eth0"

do_install_append() {
	install -d ${D}${sysconfdir}/ggsn
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -d ${D}${sysconfdir}/defaultconfig/config/ggsn

	#install configuration file
	install -m 0660 ${THISDIR}/files/ggsn.conf ${D}${sysconfdir}/defaultconfig/config/ggsn

	#create symbolic link
	ln -sf /mnt/rom/user/config/ggsn/ggsn.conf ${D}${sysconfdir}/ggsn/ggsn.conf

	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants
	install -m 0644 ${THISDIR}/files/${PN}.service ${D}${systemd_unitdir}/system/
	install -m 0644 ${THISDIR}/files/${PN}-nat@${IF}.service ${D}${systemd_unitdir}/system/

	# enable systemd on sysinit
	ln -sf ../${PN}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}-nat@${IF}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/

}

CONFFILES_${PN} = "${sysconfdir}/ggsn/ggsn.conf"

PACKAGES =+ " libgtp libgtp-dev libgtp-staticdev"

FILES_libgtp = "${libdir}/*${SOLIBS}"
FILES_libgtp-dev = "${includedir} ${libdir}/lib*${SOLIBSDEV} ${libdir}/*.la"
FILES_libgtp-staticdev = "${libdir}/*.a"

FILES_${PN} += "${systemd_unitdir}"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE_${PN} = "${PN}.service"
SYSTEMD_SERVICE_${PN} += "${PN}-nat@${IF}.service"
