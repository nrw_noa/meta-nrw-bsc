DESCRIPTION = "MNCC to SIP bridge"
HOMEPAGE = "http://git.osmocom.org/osmo-sip-connector/"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=ccdfc45f78d625db70e70ede679d5ce3"

inherit autotools pkgconfig

DEPENDS = "libosmocore sofia-sip"

SRC_URI = "git://git.osmocom.org/osmo-sip-connector;branch=master"
S = "${WORKDIR}/git"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "osmo-sip-connector_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_BRANCH  = "master"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "git://git.osmocom.org/osmo-sip-connector;branch=${DEV_BRANCH}"

REL_BRANCH  = "master"
REL_SRCREV  = "fffc742777f8942b833ab46f8b0b1499ad57713a"
REL_SRCURI := "git://git.osmocom.org/osmo-sip-connector;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_BSC_DEVEL', False) else '${REL_SRCURI}'}"

SRC_URI += " \
	file://osmo-sip-connector.cfg \
	file://osmo-sip-connector.service \
	"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

do_install () {
	install -d ${D}${sysconfdir}/osmocom
	install -d ${D}${sysconfdir}/defaultconfig
	install -d ${D}${sysconfdir}/defaultconfig/config
	install -d ${D}${sysconfdir}/defaultconfig/config/osmocom
	install -d ${D}${bindir}

	install -m 0660 ${WORKDIR}/osmo-sip-connector.cfg ${D}${sysconfdir}/defaultconfig/config/osmocom

	install -m 0755 ${WORKDIR}/build/src/${PN} ${D}${bindir}

	# create symbolic links
	ln -sf /mnt/rom/user/config/osmocom/${PN}.cfg ${D}${sysconfdir}/osmocom/${PN}.cfg

	# install systemd
	install -d ${D}${systemd_unitdir}/system
	install -m 0644 ${WORKDIR}/${PN}.service ${D}${systemd_unitdir}/system/

	# enable systemd on sysinit
	install -d ${D}${systemd_unitdir}/system/multi-user.target.wants/
	ln -sf ../${PN}.service ${D}${systemd_unitdir}/system/multi-user.target.wants/

}

FILES_${PN} = "${bindir}/${PN} \
		${sysconfdir}/defaultconfig/config/osmocom/${PN}.cfg \
		${sysconfdir}/osmocom/${PN}.cfg \
		${systemd_unitdir}/system/${PN}.service \
		${systemd_unitdir}/system/multi-user.target.wants/${PN}.service \
		"
